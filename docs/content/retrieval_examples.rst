Retrieval Examples
==================

In this section we will give fully imlemented examples of
retrievals. **Please use the** `petitRADTRANS Retrieval Tutorial <notebooks/basic_retrieval.html>`_
**from now on, we still give our old setups from the** `petitRADTRANS paper
<https://arxiv.org/abs/1904.11504>`_ **though**.

.. toctree::
   :maxdepth: 2

   notebooks/basic_retrieval
   notebooks/retrieval_multiple_data
   notebooks/emission_retrieval
   notebooks/retrieval_models
